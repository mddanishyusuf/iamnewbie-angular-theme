var app = angular.module('iamnewbie',['ui.router','ngSanitize']);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider){
	$locationProvider.html5Mode(true).hashPrefix('!')

	$stateProvider
	.state('home', {
		url: '/',
		templateUrl: templateDir.partials + 'home.php',
		controller: 'homeController'
	})

	.state('single_post', {
		url: '/blog/:post_id/:slug/',
		templateUrl: templateDir.partials + 'single.php',
		controller: 'singlePostController'
	})

	.state('category_post', {
		url: '/blog/:category_slug/category',
		templateUrl: templateDir.partials + 'home.php',
		controller: 'homeController'
	})

	.state('author_posts', {
		url: '/blog/author/:authorId/',
		templateUrl: templateDir.partials + 'home.php',
		controller: 'homeController'
	})

	.state('about', {
		url: '/about/',
		templateUrl: templateDir.partials + 'about.php'
	})

	$urlRouterProvider.otherwise('/notfound');
})