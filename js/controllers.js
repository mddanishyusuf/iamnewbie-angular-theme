app.controller('mainController', function($scope, $timeout, $http){
	$scope.footer_content = false
	$scope.showLandingPage = false
	$scope.$on('$viewContentLoaded', function(){
    	
     $timeout(function(){
     	Prism.highlightAll();
     }, 1000);      
    
    $timeout(function(){
     	$scope.footer_content = true
     }, 1000);
  });

	$http({
		method: 'GET',
		url: 'wp-json/taxonomies/category/terms'
	}).then(function(success){
		$scope.category_list = success.data
	}).finally(function(){		
		$scope.showLandingPage = true
	})
})

app.controller('homeController', function($scope, $http, $stateParams, $timeout){	
	$scope.show_category_title = false
	var home_list = []

	$scope.pagination = 1		
	$scope.loadMore = function() {
		if($stateParams.category_slug){
			var url = "wp-json/posts/?filter[category_name]=" + $stateParams.category_slug + "&page=" + $scope.pagination
			$scope.show_category_title = true
		}
		else if($stateParams.authorId){
			var url = "wp-json/posts/?filter[author]=" + $stateParams.authorId + "&page=" + $scope.pagination
		}else{
			var url = "wp-json/posts/?page=" + $scope.pagination
		}
		$http({
			method: 'GET',
			url: url
		}).then(function (success){
			angular.forEach(success.data, function(val){
				home_list.push(val)
			})	
		$scope.total_pages = success.headers('X-WP-TotalPages');
		}).catch(function(response) {
	  		console.error('Error occurred:', response.status, response.data);
		}).finally(function() {
			
		});
		$scope.home_data = home_list 
		$scope.pagination = $scope.pagination + 1;
	};
	$scope.loadMore();


	$scope.return_time = function(tm){
	    return moment(tm).fromNow()
	}
})

app.controller('singlePostController', function($scope, $http, $stateParams, $sce){
	$http({
		method: 'GET',
		url: 'wp-json/posts/'+ $stateParams.post_id
	}).then(function(success){
		$scope.single_data = success.data
		$scope.trusted_content =  $sce.trustAsHtml(success.data.content)
	})
	$scope.return_time = function(tm){
	    return moment(tm).fromNow()
	}
	$scope.author_date = function(tm){
		return moment(tm).format('MMM Do YY')
	}
})