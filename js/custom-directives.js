app.directive('ianHeader', function(){
	return {
		restrict: 'E',
		templateUrl: templateDir.partials + 'header.php'
	};
});

app.directive('ianFooter', function(){
	return {
		restrict: 'E',
		templateUrl: templateDir.partials + 'footer.php'
	};
});