<div class="container-fluid">
	<div class="row header">
<div class="col-xs-10 col-md-4 title-logo" ui-sref="home">
<h1>IAMNEWBIE</h1>
<h4>A Talk About Open Source Projects & APIs</h4>  
</div>
<div class="col-xs-2 hidden-md hidden-lg">
<i class="fa fa-bars" onclick="openNav()"></i>
<div id="mobileNav" class="overlay">
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	 <div class="overlay-content">
    <a href="#" ng-repeat="category in category_list">{{category.name}}</a>
  </div>
</div>
</div>
<div class="col-md-8 page-category-list hidden-xs hidden-sm">
  <ul>
    <li ng-repeat="category in category_list"><a href="blog/{{category.slug}}/category">{{category.name}}</a></li>
  </ul>
</div>
</div>
</div>