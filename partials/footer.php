<div class="container-fluid footer" ng-show="footer_content">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-xs-6">
				<ul>
					<li ng-repeat="category in category_list"><a ui-sref="category_post({category_slug:category.slug})">{{category.name}}</a></li>
				</ul>
			</div>
			<div class="col-md-2 col-xs-6">
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>
			<div class="col-md-offset-1 col-md-7 col-xs-12 about-web">
				<p>
					IAMNEWBIE is created, written and maintained by <a href="https://github.com/mddanishyusuf" target="_blank">Mohd Danish Yusuf</a>. this blog is based on wordpress CMS and theme coded with <a href="http://v2.wp-api.org/" target="_blank">WP REST API</a> + <a href="https://angularjs.org/" target="_blank">AngularJS</a> + <a href="http://getbootstrap.com/" target="_blank">Bootstrap</a> with lot of experience and hard work. Really SPA is Awesome and interesting things as data-binding, custom directives, filters and lots of things to learn in AngularJS.
				</p>
				<div class="social-links">					
					<a href=""><i class="fa fa-facebook-official"></i></a>
					<a href=""><i class="fa fa-twitter"></i></a>
					<a href=""><i class="fa fa-youtube"></i></a>
					<a href=""><i class="fa fa-slack"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>