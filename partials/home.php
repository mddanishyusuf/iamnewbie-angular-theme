<br>
<div class="container home-page">
<div ng-if="show_category_title">
<h3 class="category-title">Category : {{home_data[0].terms.category[0].name ? home_data[0].terms.category[0].name : 'No Post related to this category'}}</h3>
<hr>
</div>
<div ng-repeat="post in home_data">
	<div class="row">
	<div class="col-md-10">
	<div class="row post-class">
		<div class="col-md-5" ui-sref="single_post({post_id:post.ID,slug:post.slug})">
			<img data-src="{{post.featured_image.source}}" class="lazyload" style="width: 100%">
		</div>
		<div class="col-md-7">
			<a ui-sref="single_post({post_id:post.ID,slug:post.slug})"><h3>{{post.title}}</h3></a>
			<div class="row">
				<div class="col-md-12 post-meta-data">
					<ul>
						<li><i class="fa fa-calendar"></i> {{return_time(post.date)}}</li>
						<li><a ui-sref="author_posts({authorId:post.author.ID})"><i class="fa fa-user"></i> {{post.author.name}}</a></li>
						<li><a ui-sref="category_post({category_slug:post.terms.category[0].slug})"><i class="fa fa-tags"></i> {{post.terms.category[0].name}}</a></li>
					</ul>
				</div>
			</div>
			<hr>
			<p ng-bind-html="post.excerpt"></p>
			<br>
			<a ui-sref="single_post({post_id:post.ID,slug:post.slug})" class="read-text"><i class="fa fa-arrow-right read-more"></i> <span>Read More</span></a>
		</div>
	</div>
	<hr>
	</div>
	</div>
</div>
<center class="load-more"><button type="button" ng-disabled="total_pages == pagination - 1" class="btn btn-default more-video" ng-click="loadMore()">{{total_pages == pagination - 1 ? 'No More Post' : 'Load More'}}</button></center>
</div>