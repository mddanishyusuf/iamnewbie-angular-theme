<div class="container">
	<div class="row single-post">
		<div class="col-md-12 text-center">
			<h2>{{single_data.title}}</h2>
			<div class="author-meta">
				<a><span ng-bind="return_time(single_data.date)"></span></a> <span class="hidden-xs">by</span> 
				<a ui-sref="author_posts({authorId:single_data.author.ID})" class="hidden-xs">{{single_data.author.name}}</a> in
				<a ui-sref="category_post({category_slug:single_data.terms.category[0].slug})">{{single_data.terms.category[0].name}}</a>
			</div>
			<div class="featured-image">
				<img src="{{single_data.featured_image.attachment_meta.sizes.medium_large.url}}">
			</div>
		</div>
	</div>
	<br>
	<div class="row single-post-content">
		<div class="col-md-offset-2 col-md-8">
			<p ng-bind-html="trusted_content"></p>
		</div>
	</div>
<hr>
	<div class="row author">
		<div class="col-md-offset-1 col-md-1">
			<img src="{{single_data.featured_image.author.avatar}}">
		</div>
		<div class="col-md-9">
			<h4><a ui-sref="author_posts({authorId:single_data.featured_image.author.ID})">{{single_data.featured_image.author.name}}</a></h4>
			<p>{{single_data.featured_image.author.description}}</p>
			<!-- <div class="autor-icon">
				<i class="fa fa-globe"></i> {{single_data.featured_image.author.URL}}
				<a ui-sref="author_posts({authorId:single_data.featured_image.author.ID})">All post by {{single_data.featured_image.author.username}} <i class="fa fa-angle-right"></i>
			</div> -->
		</div>
	</div>
</div>