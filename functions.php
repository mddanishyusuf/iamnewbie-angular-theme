<?php 
	function add_libraries(){
		wp_enqueue_script('jquery', get_template_directory_uri(). '/node_modules/jquery/dist/jquery.min.js');

		wp_enqueue_script('angularjs', get_template_directory_uri(). '/node_modules/angular/angular.min.js');

		wp_enqueue_script('ui-router', get_template_directory_uri(). '/node_modules/angular-ui-router/release/angular-ui-router.min.js');

		wp_enqueue_script('bootstrapjs', get_template_directory_uri(). '/node_modules/bootstrap/dist/js/bootstrap.min.js');
		
		wp_enqueue_script('ngsanitize', get_template_directory_uri(). '/node_modules/angular-sanitize/angular-sanitize.min.js');
		
		wp_enqueue_script('momentjs', get_template_directory_uri(). '/node_modules/moment/min/moment.min.js');

		wp_enqueue_script('lazysizes', get_template_directory_uri(). '/node_modules/lazysizes/lazysizes.min.js');
		
		wp_enqueue_script('prism', get_template_directory_uri(). '/js/script.js');

		wp_enqueue_script('script', get_template_directory_uri(). '/js/prism.js');

		wp_enqueue_script('controllers', get_template_directory_uri(). '/js/controllers.js');

		wp_enqueue_script('custom-script', get_template_directory_uri(). '/js/custom-directives.js');

		wp_enqueue_script('infinite-scroll', get_template_directory_uri(). '/js/ng-infinite-scroll.min.js');

		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/node_modules/bootstrap/dist/css/bootstrap.min.css');
		
		wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/node_modules/font-awesome/css/font-awesome.min.css');
		
		wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');

		// wp_enqueue_style( 'prism', get_template_directory_uri() . '/css/prism.css');

		wp_enqueue_style( 'googlefont', 'https://fonts.googleapis.com/css?family=Roboto+Slab|Vollkorn');
		
		wp_localize_script(
			'script',
			'templateDir',
			array(
				'partials' => trailingslashit( get_template_directory_uri() ) . 'partials/'
				)
		);
	}

	add_action('wp_enqueue_scripts', 'add_libraries');

	add_theme_support( 'post-thumbnails' ); 
?>