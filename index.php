<!DOCTYPE html>
<html>
<head>
	<base href="/">
	<title>IAMNEWBIE | A Talk About Open Source Projects & APIs</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body ng-app="iamnewbie" id="mainBody" ng-controller="mainController" ng-show="showLandingPage">
<ian-header></ian-header>
<ui-view></ui-view>
<ian-footer></ian-footer>
<script type="text/javascript">
	/* Open when someone clicks on the span element */
function openNav() {
    document.getElementById("mobileNav").style.width = "100%";
    document.getElementById("mainBody").style.overflowY = "hidden";
}

/* Close when someone clicks on the "x" symbol inside the overlay */
function closeNav() {
    document.getElementById("mobileNav").style.width = "0%";
    document.getElementById("mainBody").style.overflowY = "scroll";
}
</script>
</body>
</html>